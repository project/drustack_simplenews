api = 2
core = 7.x

; Modules
projects[mailsystem][download][tag] = 7.x-2.34
projects[mailsystem][download][type] = git
projects[mailsystem][subdir] = contrib
projects[mimemail][download][branch] = 7.x-1.x
projects[mimemail][download][type] = git
projects[mimemail][subdir] = contrib
projects[simplenews][download][tag] = 7.x-1.1
projects[simplenews][download][type] = git
projects[simplenews][subdir] = contrib
